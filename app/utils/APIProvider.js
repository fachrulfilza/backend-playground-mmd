import axios from 'axios';
import RNFetchBlob from 'rn-fetch-blob';
import AsyncStorage from '@react-native-community/async-storage';
import { 
    BlockActions,
    InsentifActions,
    MaterialActions,
    WorkerActions,
    VendorActions,
    EOCActions,
    BlockPrevActions
 } from '@psmi-storage/realm';

const baseUrl = 'https://app.psmi.co.id';
// const baseUrl = 'https://dev.rightsolutions.id';
const EXTERNAL_DIR = RNFetchBlob.fs.dirs.DownloadDir;
const APP_DIR  = RNFetchBlob.fs.dirs.DocumentDir;

const master_file = [
    {model: 'insentif', file: 'master_insentif.json'},
    {model: 'eoc', file: 'master_resource.json'},
    {model: 'material', file: 'master_material.json'},
    {model: 'vendor', file: 'master_vendor.json'},
    {model: 'worker', file: 'master_worker.json'},
    {model: 'block', file: 'master_blocks.json'},
]

export const InitializeMasterData = () => {
    master_file.map((item) => {
        let path = RNFetchBlob.fs.asset(`master_data/${item.file}`);
        let targetPath = `${APP_DIR}/${item.file}`;
        return RNFetchBlob.fs.exists(targetPath)
        .then((exist) => {
            if(!exist) {
                return RNFetchBlob.fs.cp(path, targetPath).then(() => {
                    return readAndInsertMaster(targetPath, item.model);
                })
            }else{
                return readAndInsertMaster(targetPath, item.model);
            }
        })
    });
}

readAndInsertMaster = (targetPath, model) =>{
    return RNFetchBlob.fs.readFile(targetPath, 'utf8').then((res) => {
        const data = JSON.parse(res).data;
        switch(model){
            case 'insentif': 
                return InsentifActions.bulkInsert(data);
            case 'material': 
                return MaterialActions.bulkInsert(data);
            case 'worker': 
                return WorkerActions.bulkInsert(data);
            case 'vendor': 
                return VendorActions.bulkInsert(data);
            case 'block':
                return BlockActions.bulkInsert(data);
            case 'eoc':
                return EOCActions.bulkInsert(data);
            default:
                return data;
        }
    })
}

export const DailyPlan = {
    /* Process Get Daily Plan
     * @params => opr, last_synch
     * opr => Worker ID
    */
    getDataStorage: () => {
        return AsyncStorage.getItem('TASK_ORDER:LIST');
    },
    /* Fetch Daily Plan Data
     * @params => opr, last_synch
     * opr => Worker ID
    */
    fetchData: (reqParam) => new Promise((resolve, reject) => {
        axios.get(`${baseUrl}/api/synchronize/dailyplan`, {
            params: reqParam
        }).then(response => {
            const data = response.data.data;
            if(data.length > 0){
                DailyPlan.getDataStorage().then((res) => {

                    const oldTOs = res != null ? JSON.parse(res) : []; 
                    let result = [];
                    data.map((newTO) => {
                        let index = oldTOs.map(oldTO => oldTO.task_order_id).indexOf(newTO.task_order_id);
                        if(index == -1) result.push(newTO);
                    })
                    
                    const newTOs = oldTOs.concat(result);
                    AsyncStorage.setItem('TASK_ORDER:LIST', JSON.stringify(newTOs));
                    resolve(newTOs);
                })
            }else{
                resolve(data);
            }
        }).catch((error) => {
            reject(error);
        })  
    })
}

export const DailyActivity = {
    // Submit Daily Activity
    post: (taskOrder) => {
        delete taskOrder._refresh;
        let data = {...taskOrder};
        data = {
            ...data,
            labors: data.labors.length > 0 ? parseLaborDate(data.labors) : [], 
            insentif: data.insentif.length > 0 ? parseInsentif(data.insentif) : [],
            materials: data.materials.length > 0 ? parseMaterial(data.materials) : [],
            blocks: data.blocks.length > 0 ? parseBlock(data.blocks) : [],
            eoc: data.eoc.length > 0 ? parseEOC(data.eoc) : [],
            contracts: data.contracts.length > 0 ? parseContract(data.contracts) : []
        }
        return axios.post(`${baseUrl}/api/synchronize/DailyPlan`, data);
    },
}

export const Worker = {
    /* Get Master Worker Data
    */
    getData: () => {
        return WorkerActions.retrieveAll();
    },
    /* Fetch Master Worker Data
     * @params => last_sync
    */
    fetchData: (reqParam) => new Promise((resolve, reject) => {
        axios.get(`${baseUrl}/api/synchronize/worker`, {
            params: reqParam
        }).then(response => {
            const data = response.data.data;
            if(data.length > 0) WorkerActions.bulkInsert(data)
            resolve(data);
        }).catch((error) => {
            reject(error);
        })  
    })
}

export const BlockPrev = {
    /* Get Master Worker Data
    */
    getData: () => {
        return BlockPrevActions.retrieveAll();
    },
    /* Fetch Master Worker Data
     * @params => last_sync
    */
    fetchData: (reqParam) => new Promise((resolve, reject) => {
        axios.get(`${baseUrl}/api/synchronize/blockPrevBalance`, {
            params: reqParam
        }).then(response => {
            const data = response.data.data;
            if(data.length > 0) BlockPrevActions.bulkInsert(data)
            resolve(data);
        }).catch((error) => {
            reject(error);
        })  
    })
}

export const Material = {
    /* Get Master Material Data
    */
    getData: () => {
        return MaterialActions.retrieveAll();
    },
    /* Fetch Master Material Data
     * @params => last_sync
    */
    fetchData: (reqParam) => new Promise((resolve, reject) => {
        axios.get(`${baseUrl}/api/synchronize/material`, {
            params: reqParam
        }).then(response => {
            const data = response.data.data;
            if(data.length > 0) MaterialActions.bulkInsert(data)
            resolve(data);
        }).catch((error) => {
            reject(error);
        })  
    })
}

export const Resource = {
    /* Get Master eoc Data
    */
    getData: () => {
        return EOCActions.retrieveAll();
    },
    /* Fetch Master eoc Data
     * @params => last_sync
    */
    fetchData: (reqParam) => new Promise((resolve, reject) => {
        axios.get(`${baseUrl}/api/synchronize/resource`, {
            params: reqParam
        }).then(response => {
            const data = response.data.data;
            if(data.length > 0) EOCActions.bulkInsert(data)
            resolve(data);
        }).catch((error) => {
            reject(error);
        })  
    })
}

export const Vendor = {
    /* Get Master Vendor Data
    */
    getData: () => {
        return VendorActions.retrieveAll();
    },
    /* Fetch Master Vendor Data
     * @params => last_sync
    */
    fetchData: (reqParam) => new Promise((resolve, reject) => {
        axios.get(`${baseUrl}/api/synchronize/vendor`, {
            params: reqParam
        }).then(response => {
            const data = response.data.data;
            if(data.length > 0) VendorActions.bulkInsert(data)
            resolve(data);
        }).catch((error) => {
            reject(error);
        })  
    })
}

export const Divisi = {
    /* Get Master Vendor Data
     * @params => last_synch
    */
    getData: (reqParam) => {
        return axios.get(`${baseUrl}/api/synchronize/vendor`, {
            params: reqParam
        });
    },
}

export const Insentif = {
    /* Get Master Insentif Data
    */
    getData: () => {
        return InsentifActions.retrieveAll();
    },
    /* Fetch Master Insentif Data
     * @params => last_sync
    */
    fetchData: (reqParam) => new Promise((resolve, reject) => {
        axios.get(`${baseUrl}/api/synchronize/insentif`, {
            params: reqParam
        }).then(response => {
            const data = response.data.data;
            if(data.length > 0) InsentifActions.bulkInsert(data)
            resolve(data);
        }).catch((error) => {
            reject(error);
        })  
    })
}

export const Block = {
    /* Get Master Block Data
    */
    getData: () => {
        return BlockActions.retrieveAll();
    },
    /* Fetch Master Block Data
     * @params => last_sync
    */
    fetchData: (reqParam) => new Promise((resolve, reject) => {
        axios.get(`${baseUrl}/api/synchronize/blocks`, {
            params: reqParam
        }).then(response => {
            const data = response.data.data;
            if(data.length > 0) BlockActions.bulkInsert(data)
            resolve(data);
        }).catch((error) => {
            reject(error);
        })  
    })
}

export const UOM = {
    /* Get Master Unit of Measure Data
     * @params => last_synch
    */
    getData: (reqParam) => {
        return axios.get(`${baseUrl}/api/synchronize/uom`, {
            params: reqParam
        });
    },
}

export const User = {
    loginAxios: (userid, password) => {
        return axios.post(`http://app.psmi.co.id/api/auth2/loginnew?userid=${userid}&password=${password}`);
    },
    login: (userid, password) => {
        return RNFetchBlob
                .config({ timeout: 6500 })
                .fetch('POST', `https://app.psmi.co.id/api/auth2/loginnew?userid=${userid}&password=${password}`, {});
    }
}


_getMasterData = (path) => {
    return RNFetchBlob.fs.exists(path)
    .then((exist) => {
        if(exist) {
            return RNFetchBlob.fs.readFile(path, 'utf8');
        }else{
            return null;
        }
    })
}
_fetchMasterDataToJSON = async (url, model) => {
    const EXTERNAL_PATH = `${EXTERNAL_DIR}/master_${model}.json`;
    const APP_PATH = `${APP_DIR}/master_${model}.json`;
    return RNFetchBlob.fs.exists(EXTERNAL_PATH).then((exist) => {
        if(exist){
            //DELETE SEMUA FILE YANG ADA DI DIRECTORY EXTERNAL (DOWNLOADS)
            return RNFetchBlob.fs.unlink(EXTERNAL_PATH).then(() => {
                return __downloadData(url, model, APP_PATH, EXTERNAL_PATH);
            });
        }else{
            return __downloadData(url, model, APP_PATH, EXTERNAL_PATH);
        }
    }).catch((error) => {
        console.log(error);
    })
}
__downloadData = (url, model, APP_PATH, EXTERNAL_PATH) => {
    const config = {
        // addAndroidDownloads : {
        //     useDownloadManager : true,
        //     title : `Master ${model}`,
        //     notification : true,
        //     mime : 'application/json',
        //     description : 'Downloading master data',
        //     path : EXTERNAL_PATH,
        // },
        // fileCache: true,
        // appendExt: 'png',
        path : EXTERNAL_PATH,
        out: 10000
    };
    return RNFetchBlob
    .config(config)
    .fetch("GET", url)
    .then((res) => {
        return RNFetchBlob.fs.readFile(EXTERNAL_PATH, 'utf8')
        .then((data) => {
            if(JSON.parse(data).data.length > 0){
                return RNFetchBlob.fs.exists(APP_PATH)
                .then((exist) => {
                    if(exist) {
                        return RNFetchBlob.fs.unlink(APP_PATH)
                        .then(() => {
                            return RNFetchBlob.fs.mv(EXTERNAL_PATH, APP_PATH)
                            .then(() => {
                                return RNFetchBlob.fs.readFile(APP_PATH, 'utf8')
                            });    
                        })
                    }else{
                        return RNFetchBlob.fs.mv(EXTERNAL_PATH, APP_PATH)
                        .then(() => {
                            return RNFetchBlob.fs.readFile(APP_PATH, 'utf8')
                        });
                    }
                })
            }
            return RNFetchBlob.fs.readFile(EXTERNAL_PATH, 'utf8');
        })
    }).catch((error) => {
        return error;
    })
}

parseInsentif = (insentif) => {
    let parsedInsentif = insentif;
    parsedInsentif.map((item) => {
        item.desc = item.description;
    });

    return parsedInsentif;
} 

parseEOC = (eoc) => {
    let parsedEOC = eoc;
    parsedEOC.map((item) => {
        item.hm = item.hm > 0 ? parseFloat(item.hm) : 0;
        item.standby = item.standby > 0 ? parseFloat(item.standby) : 0;
        item.km = item.km > 0 ? parseFloat(item.km) : 0;
        item.result = item.result > 0 ? parseFloat(item.result) : 0;
        if(item.drivers != null && item.drivers.length > 0){
            item.drivers = parseDriverDate(item.drivers);
        }else{
            item.drivers = [];
        }
        item.implements = (item.implements != null && item.implements.length > 0) ? item.implements : [];
    })
    
    return parsedEOC;
}

parseContract = (contracts) => {
    let parsedContract = contracts;
    parsedContract.map((item) => {
        item.crew = (item.crew != null && item.crew.length > 0) ? item.crew : [];
    })
    
    return parsedContract;
}

parseBlock = (blocks) => {
    let parsedBlock = blocks;
    parsedBlock.map((block) => {
        block.prev_balance = block.prev_balance > 0 ? parseFloat(block.prev_balance) : 0;
        block.result = block.result > 0 ? parseFloat(block.result) : 0;
        block.balance = block.balance > 0 ? parseFloat(block.balance) : 0;
    })
    
    return parsedBlock;
}
parseMaterial = (materials) => {
    let parsedMaterial = materials;
    parsedMaterial.map((material) => {
        material.qty = material.qty > 0 ? parseFloat(material.qty) : 0;
    })
    
    return parsedMaterial;
}

parseLaborDate = (labors) => {
    let parsedLabors = labors;
    parsedLabors.map((labor) => {
        labor.timein = _parseDate(labor.in);
        labor.timeout = _parseDate(labor.out);
    })
    
    return parsedLabors;
}
parseDriverDate = (drivers) => {
    let parsedDriver = drivers;
    parsedDriver.map((driver) => {
        driver.timein = _parseDate(driver.in);
        driver.timeout = _parseDate(driver.out);
    })
    return parsedDriver;
}

_parseDate = (req) => {
    let pad = function(num) { return ('00'+num).slice(-2) };
    let date = new Date(req);
    if(date.getTime()){
        date = pad(date.getHours())+ ':' +pad(date.getMinutes());
        return date;
    }
    return '00:00';
        
} 