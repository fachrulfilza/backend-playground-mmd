import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { 
    IntroScreen,
} from '../screens';

const StackNavigator = createStackNavigator({
    IntroScreen: {
        screen: IntroScreen,
        navigationOptions: ({ navigation }) => ({
            header: null,
        }),
    },
}, { 
    initialRouteName: 'IntroScreen',
    defaultNavigationOptions: {
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontSize: 16,
          fontFamily: "Montserrat-Regular"
        },
    },
});

const AppNavigator = createAppContainer(StackNavigator);

export default AppNavigator;