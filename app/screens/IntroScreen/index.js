import React, { Component } from "react";
import {
    GoogleSignin,
    GoogleSigninButton,
    statusCodes,
  } from '@react-native-community/google-signin';
import { StyleSheet, Text, View } from 'react-native';

class IntroScreen extends Component {
    constructor(props){
        super(props);
        
        this.state = {
            userInfo: {},
            isSigninInProgress: null
        };
    }

    componentDidMount = () => {
        GoogleSignin.configure({});
    }

    _signIn = async () => {
        try {
          await GoogleSignin.hasPlayServices();
          const userInfo = await GoogleSignin.signIn();
          this.setState({ userInfo }, () => {
            //   console.log(this.state.userInfo);
              GoogleSignin.getTokens().then((response) => {
                  console.log(response);
              })
          });
        } catch (error) {
          if (error.code === statusCodes.SIGN_IN_CANCELLED) {
            // user cancelled the login flow
            console.log('SIGN_IN_CANCELLED');
          } else if (error.code === statusCodes.IN_PROGRESS) {
            // operation (e.g. sign in) is in progress already
            console.log('SIGN_IN_CANCELLED');
          } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
            // play services not available or outdated
            console.log('PLAY_SERVICES_NOT_AVAILABLE');
          } else {
              console.log(error);
          }
        }
    };

    getCurrentUserInfo = async () => {
        try {
          const userInfo = await GoogleSignin.signInSilently();
          this.setState({ userInfo });
        } catch (error) {
          if (error.code === statusCodes.SIGN_IN_REQUIRED) {
            // user has not signed in yet
          } else {
            // some other error
          }
        }
    };

    render = () => {
        return (
            <View style={styles.container}>
                <Text>React Native Boilerplate - Muhza 2020</Text>
                <GoogleSigninButton
                    style={{ width: 192, height: 48 }}
                    size={GoogleSigninButton.Size.Wide}
                    color={GoogleSigninButton.Color.Dark}
                    onPress={this._signIn.bind(this)}
                    disabled={this.state.isSigninInProgress} 
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
});

export default IntroScreen;